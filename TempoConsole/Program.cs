﻿using ProjectoTempoDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TempoConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Tempo t = new Tempo();
            t.Hora = 10;
            t.Minutos = 20;
            t.Segundos = 59;
            Console.WriteLine(t.ToString());
            Console.ReadKey();
        }
    }
}
