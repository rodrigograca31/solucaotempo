﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectoTempoDLL
{
    public class Tempo
    {
        private int hora;
        private int minutos;
        private int segundos;
        public int Hora
        {
            set
            {
                if(value >=0 && value < 24){
                    this.hora = value;
                }else {
                    throw new Exception("Hora invaldia");
                }
            }
            get
            {
                return hora;
            }
        }
        public int Minutos
        {
            set
            {
                if (value >= 0 && value < 60)
                {
                    this.minutos = value;
                }
                else
                {
                    throw new Exception("Minuto invaldio");
                }
            }
            get
            {
                return minutos;
            }
        }
        public int Segundos
        {
            set
            {
                if (value >= 0 && value < 60)
                {
                    this.segundos = value;
                }
                else
                {
                    throw new Exception("Segundo invaldio");
                }
            }
            get
            {
                return segundos;
            }
        }
        public override string ToString()
        {
            return string.Format("{0}h {1}m {2}s", this.hora, this.minutos, this.segundos);
        }
    }
}
